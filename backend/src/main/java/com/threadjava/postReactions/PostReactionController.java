package com.threadjava.postReactions;

import com.threadjava.post.PostsService;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postsReactionService;
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        var reaction = postsReactionService.setReaction(postReaction);
        var post = postsService.getPostById(postReaction.getPostId());

        if (reaction.isPresent() && !reaction.get().getUserId().equals(post.getUser().getId())
        && post.getUser().getId().equals(getUserId())) {
            // notify a user if someone (not himself) liked his post
            template.convertAndSend( "/topic/like", "Your post was liked!");
        }
        return reaction;
    }
}
