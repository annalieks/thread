package com.threadjava.comment;

import com.threadjava.comment.dto.CommentQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    @Query("SELECT new com.threadjava.comment.dto.CommentQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.user, c.post) " +
            "FROM Comment c " +
            "WHERE (c.post.id = :postId) AND c.isDeleted = FALSE " +
            "order by c.createdAt desc" )
    List<CommentQueryResult> findAllComments(@Param("postId") UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentQueryResult(c.id, c.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.user, c.post) " +
            "FROM Comment c " +
            "WHERE (c.id = :commentId) AND c.isDeleted = FALSE " +
            "order by c.createdAt desc" )
    CommentQueryResult findCommentById(@Param("commentId") UUID commentId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Comment c " +
            "SET c.body = :body, " +
            "updated_on = NOW() " +
            "WHERE c.id = :id")
    void updateCommentById(@Param("id") UUID id, @Param("body") String body);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Comment c " +
            "SET c.isDeleted = TRUE, " +
            "updated_on = NOW() " +
            "WHERE c.id = :id")
    void deleteById(@Param("id") UUID id);
}