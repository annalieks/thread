package com.threadjava.post.dto;

import lombok.Data;
import com.threadjava.image.dto.ImageDto;
import java.util.UUID;

@Data
public class PostUserDto {
    private UUID id;
    private String username;
    private ImageDto image;
}
