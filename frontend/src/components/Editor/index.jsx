import { Editor, EditorState, ContentState } from 'draft-js';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './styles.module.scss';

class InputEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = { editorState: EditorState.createWithContent(
      ContentState.createFromText(props.content.body)
    ),
    prevContent: props.content.body };
    this.onChange = editorState => this.setState({ editorState });
  }

  componentDidUpdate(prevProps) {
    const { readOnly, update, userId, content } = this.props;
    const { editorState } = this.state;
    if (readOnly === false && prevProps.readOnly === true) {
      this.editor.focus();
    } else if (readOnly === true && prevProps.readOnly === false) {
      update(userId, content, editorState.getCurrentContent().getPlainText('\u0001'));
    }
  }

  static getDerivedStateFromProps(nextProps, state) {
    if (nextProps.content.body !== state.prevContent) {
      return {
        editorState: EditorState.createWithContent(ContentState.createFromText(nextProps.content.body)),
        prevContent: nextProps.content.body
      };
    }
    return null;
  }

  render() {
    const { editorState } = this.state;
    const { readOnly, makeEditable } = this.props;
    return (
      <div className={readOnly ? styles.unfocused : styles.focused}>
        <Editor
          editorState={editorState}
          onChange={this.onChange}
          readOnly={readOnly}
          ref={element => { this.editor = element; }}
          onFocus={() => makeEditable(true)}
        />
      </div>
    );
  }
}

InputEditor.propTypes = {
  readOnly: PropTypes.bool.isRequired,
  content: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  makeEditable: PropTypes.func.isRequired
};

export default InputEditor;
