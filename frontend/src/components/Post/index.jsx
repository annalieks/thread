import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import InputEditor from 'src/components/Editor';

import styles from './styles.module.scss';

const Post = ({ userId, post, likePost, dislikePost,
  toggleExpandedPost, sharePost, updatePost, deletePost }) => {
  const {
    id,
    image,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const [isEditable, setIsEditable] = useState(false);
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {
            userId === post.user.id
          && (
            <Label basic size="small" as="a" className={styles.deleteBtn} onClick={() => deletePost(id)}>
              <Icon name="delete" />
            </Label>
          )
          }
        </Card.Meta>
        <InputEditor
          userId={userId}
          readOnly={!isEditable}
          content={post}
          update={updatePost}
          makeEditable={setIsEditable}
        />
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {
          userId === post.user.id && !isEditable
        && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => { setIsEditable(true); }}>
            <Icon name="edit" />
          </Label>
        )
        }
        {
          userId === post.user.id && isEditable
        && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEditable(false)}>
            <Icon name="save" />
          </Label>
        )
        }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
