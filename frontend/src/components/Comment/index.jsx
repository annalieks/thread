import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import InputEditor from 'src/components/Editor';

import styles from './styles.module.scss';

const Comment = ({ comment, userId, updateComment, deleteComment, likeComment, dislikeComment }) => {
  const [isEditable, setIsEditable] = useState(false);
  const {
    id,
    postId,
    user,
    createdAt,
    likeCount,
    dislikeCount
  } = comment;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
          {
            userId === user.id && !isEditable
            && (
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEditable(true)}>
                <Icon name="edit" />
              </Label>
            )
          }
          {
            userId === comment.user.id && isEditable
          && (
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEditable(false)}>
              <Icon name="save" />
            </Label>
          )
          }
          {
            userId === user.id
            && (
              <Label
                basic
                size="small"
                as="a"
                className={styles.deleteBtn}
                onClick={() => deleteComment(comment.id, comment.postId)}
              >
                <Icon name="delete" />
              </Label>
            )
          }
        </CommentUI.Metadata>
        <InputEditor
          userId={userId}
          readOnly={!isEditable}
          content={comment}
          update={updateComment}
          makeEditable={setIsEditable}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => { likeComment(id, postId); }}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => { dislikeComment(id, postId); }}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
