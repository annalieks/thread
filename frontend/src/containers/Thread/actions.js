import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const likeDiff = result?.id ? 1 : -1;
  const dislikeDiff = result?.wasLike === false ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const dislikeDiff = result?.id ? 1 : -1;
  const likeDiff = result?.wasLike === true ? -1 : 0;

  const mapDislikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const updatePost = (userId, post, body) => async (dispatch, getRootState) => {
  if (userId !== post.user.id) return;

  await postService.update(post.id, body);
  const mapBody = singlePost => ({
    ...singlePost,
    body
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(singlePost => (singlePost.id !== post.id ? singlePost : mapBody(singlePost)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === post.id) {
    dispatch(setExpandedPostAction(mapBody(expandedPost)));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(post => post.id !== postId);

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(null));
  }
};

export const deleteComment = (commentId, postId) => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);

  const getComments = comments => (comments.filter(comment => comment.id !== commentId));
  const mapComments = post => ({
    ...post,
    commentCount: post.commentCount - 1,
    comments: getComments(post.comments)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId
    ? post
    : { ...post,
      commentCount: post.commentCount - 1 }));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (userId, comment, body) => async (dispatch, getRootState) => {
  if (userId !== comment.user.id) return;

  await commentService.update(comment.id, body);

  const getComments = comments => (comments.map(commentElem => (commentElem.id === comment.id
    ? {
      ...commentElem,
      body
    }
    : commentElem)));

  const mapComments = post => ({
    ...post,
    comments: getComments(post.comments)
  });

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : ({
      ...post,
      commentCount: Number(post.commentCount) + 1
    })));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = (id, postId) => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(id);
  const likeDiff = result?.id ? 1 : -1;
  const dislikeDiff = result?.wasLike === false ? -1 : 0;
  const getComments = comments => (comments.map(commentElem => (commentElem.id === id
    ? ({
      ...commentElem,
      likeCount: Number(commentElem.likeCount) + likeDiff,
      dislikeCount: Number(commentElem.dislikeCount) + dislikeDiff
    })
    : commentElem)));

  const mapComments = post => ({
    ...post,
    comments: getComments(post.comments)
  });

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const dislikeComment = (id, postId) => async (dispatch, getRootState) => {
  const result = await commentService.dislikeComment(id);
  const dislikeDiff = result?.id ? 1 : -1;
  const likeDiff = result?.wasLike === true ? -1 : 0;
  const getComments = comments => (comments.map(commentElem => (commentElem.id === id
    ? ({
      ...commentElem,
      likeCount: Number(commentElem.likeCount) + likeDiff,
      dislikeCount: Number(commentElem.dislikeCount) + dislikeDiff
    })
    : commentElem)));

  const mapComments = post => ({
    ...post,
    comments: getComments(post.comments)
  });

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
